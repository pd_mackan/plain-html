var gulp = require('gulp');
var handlebars = require('gulp-compile-handlebars');
var rename = require('gulp-rename');
var dataObject = require('./src/dataObject.json');



gulp.task('start_watching', function(){
    gulp.watch('src/**/*.*',['build_site']);
})

//gulp.watch('src/**/*.handlebars',['build_site']);

gulp.task('build_site', function () {
    var templateData = {
        data: dataObject
    },
    options = {
        ignorePartials: true, //ignores the unknown footer2 partial in the handlebars template, defaults to false 
        partials: {
            footer: '<footer>the end</footer>'
        },
        batch: ['./src/partials'],
        helpers: {
            capitals: function (str) {
                return str.toUpperCase();
            }
        }
    }

    console.log(templateData.data);

    for (var i = 0; i < templateData.data.allSites.length; i++) {

        var handlebarsFilePath = 'src/' + templateData.data.allSites[i] + '.handlebars';
        var htmlFileName = templateData.data.allSites[i] + '.html';

        gulp.src(handlebarsFilePath)
        .pipe(handlebars(templateData, options))
        .pipe(rename(htmlFileName))
        .pipe(gulp.dest('public'));
    };
});